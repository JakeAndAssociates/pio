import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import {Table, Input, Label} from 'reactstrap';
import './App.css';
import logo from "./logo.svg";


export class Painter extends Component{

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      paintings: [],
      sortedPaintings: [],
      search: "",
      sorting: {
        id: 1,
        name: 1,
        price: 1,
        buyer: 1,
        data: 1
      },
      isLoading: true
    }
  }

  async componentDidMount() {
    const{id} = this.props.match.params;
    this.getPaintings(id);
  }

  async getPaintings(id) {
    const response = await fetch('/api/Paintings/' + id, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    });
    if (response.status === 200) {
      let body = await response.json();
      this.setState({paintings: body.payloads, sortedPaintings: body.payloads, isLoading: false});
    }
}

  _onChange = async (e) => {
    const {value} = e.currentTarget;
    const search = value;

    let sortedPaintings = this.state.paintings.filter(painting => painting.name.includes(value));

    this.setState({sortedPaintings: sortedPaintings, search: search});
  };

  _onClick = async (value) => {
    let sorting = this.state.sorting;
    let sortedPaintings = this.state.sortedPaintings.sort((a,b) => sorting[value] * (a[value] > b[value] || b[value] === null ? -1 : 1));

    sorting[value] = - sorting[value];
    this.setState({sortedPaintings: sortedPaintings, sorting: sorting});
  };

  render() {
    const {sortedPaintings, isLoading, search} = this.state;
    const {_onChange} = this;
    const {_onClick} = this;
    const elementPaintings = sortedPaintings.map(painting =>
        <tr key={painting.id}>
          <th scope="row">{painting.id}</th>
          <td>{painting.name}</td>
          <td>{painting.price}</td>
          {painting.buyer !== null
              ? <td>Sold Out</td>
              : <td>Available</td>
          }
          <td><img src={`data:image/jpeg;base64,${painting.data}`} width={50} height={50}/></td>
        </tr>
    );
    if (isLoading) {
      return (
          <div className="App1">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo"/>
            </header>
          </div>);
    }
    return (
        <div>
          <header className="Back">
            <div>
              <NavigationBar/>
            </div>
            <div className="text-right">
              <Input
                  name="search"
                  value={search}
                  onChange={_onChange}
                  placeholder="search"
                  style={{width: 300}}
              />
            </div>
            <div>
              <Table dark>
                <thead>
                <tr>
                  <th><Label onClick={() => _onClick("id")}>Id</Label></th>
                  <th><Label onClick={() => _onClick("name")}>Name</Label></th>
                  <th><Label onClick={() => _onClick("price")}>Price</Label></th>
                  <th><Label onClick={() => _onClick("buyer")}>Action</Label></th>
                  <th><Label onClick={() => _onClick("data")}>Sample</Label></th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                {elementPaintings}
                </tbody>
              </Table>
            </div>
          </header>
        </div>
    );
  }
}
   
export default withRouter(withCookies(Painter));

