import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import Register from './RegisterComponent';
import Users from './UsersComponent'
import Home from './HomeComponent'
import Login from './LoginComponent'
import User from './UserComponent'
import Paintings from './PaintingsComponent'
import UpdatePassword  from './UpdatePasswordComponment'
import PaintingUpload from './PaintingUploadComponent'
import UpdateProfile from "./UpdateProfileComponent";
import Painters from "./PaintersComponent";
import Painter from "./PainterComponent";
import AddFunds from "./AddFundsComponent";

export class App extends Component { 
  
  render() {
    return (
      <CookiesProvider>
          <Router>
            <Switch>
              <Route path = '/' exact={true} component = {Login}/>
              <Route path = '/register' exact={true} component = {Register}/>
              <Route path = '/home' exact={true} component = {Home}/>
              <Route path = '/users' exact={true} component = {Users}/>
              <Route path = '/paintings' exact={true} component = {Paintings}/>
              <Route path = '/painters' exact={true} component = {Painters}/>
              <Route path='/user' component={User}/>
              <Route path = '/updatepwd' exact={true} component = {UpdatePassword}/>
              <Route path = '/updateprf' exact={true} component = {UpdateProfile}/>
              <Route path='/upload' component={PaintingUpload}/>
              <Route path='/painter/:id' component={Painter}/>
              <Route path='/addFunds' component={AddFunds}/>
            </Switch>
          </Router>
        </CookiesProvider>
    );
  }
}

export default App;
