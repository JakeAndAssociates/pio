import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Col, Form,FormGroup, Label, Input,Button } from 'reactstrap';
import './App.css';
import  NavigationBar  from './NavigationBarComponent';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';


export class UpdateProfile extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    emptyUser = {
        newAddress:'',
        newPhone:'',
    };

    constructor(props) {
        super(props);
        this.state = {
            message:'',
            user:{
                newAddress:'',
                newPhone:'',
            },
        };
    }

    _onChange = (e) => {
        const { name, value} = e.currentTarget;
        let user = {...this.state.user};
        user[name] = value;
        this.setState({
            user
        });
    };

    _verify() {
        if(this.state.user.newAddress === '' && this.state.user.newPhone === '' ){
            return false;
        }
        return true;
    }

    _onSubmit = async (e) =>  {
        e.preventDefault();
        const {user} = this.state;
        const {cookies} = this.props;
        const request = {
            username : cookies.get('username'),
            newAddress : user.newAddress,
            newPhone : user.newPhone,
        };
        if(this._verify()){
            const response = await fetch('/api/updateProfile', {
                method: 'POST',
                headers: {
                    'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request),
            });

            if(response.status !== 401){
                let body = await response.json();
                this.setState({
                    message : body.errorMessage,
                    user : this.emptyUser
                });
            }else{
                this.props.history.push('/')
            }
        }
        else{
            this.setState({
                message :' Empty!'
            });
        }
    }

    render(){
        const { _onChange, _onSubmit } = this;
        const { user } = this.state;

        return(


            <div className = "background">
                <NavigationBar/>
                <Container className="App">
                    <h2>Update Profile</h2>
                    <Form className="form" onSubmit={_onSubmit}>
                        <Col>
                            <FormGroup>
                                <Label>New Address</Label>
                                <Input
                                    name = "newAddress"
                                    value = {user.newAddress}
                                    onChange = {_onChange}
                                    placeholder = "newAddress"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>New Phone</Label>
                                <Input
                                    name = "newPhone"
                                    value = {user.newPhone}
                                    onChange = {_onChange}
                                    placeholder = "newPhone"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <div>
                            {this.state.message}
                        </div>
                        <Button type="submit">Change</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}


export default withRouter(withCookies(UpdateProfile));
