import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import {Table, Button} from 'reactstrap';

import logo from './logo.svg';
import './App.css';


export class Users extends Component{

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

    constructor(props) {
        super(props);
        this.state = {
            users: [], 
            isLoading: true
        }
    }

    async componentDidMount() {
      const { cookies } = this.props;
      if(cookies.get('userType') !== 'ADMIN'){
        this.props.history.push('/home');
      }
      else{
        const response = await fetch('/api/users', {
            method: 'GET',
            headers: {
                'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            });
            if(response.status === 200){
              let body = await response.json();
              this.setState({users: body.payloads , isLoading : false});
            }
            else{
              this.props.history.push('/')
            }
        }
    }

    _revokeAccess = async(id,active) => {
      const { cookies } = this.props;
      const request = {
          accessToken : cookies.get('accessToken'),
          id : id, 
      }
       await fetch('/api/change', {
        method: 'POST',
        headers: {
            'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request),
        })
        let users = [...this.state.users];  
        let index = users.findIndex(user => user.id === id);
        users[index].active = !active;

        const response = await fetch('/api/users', {
            method: 'GET',
            headers: {
                'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        if(response.status === 200){
            let body = await response.json();
            this.setState({users: body.payloads , isLoading : false});
        }


    }

    render() {
      const {users, isLoading} = this.state;
      const {_revokeAccess} = this;
      const elemetUsers = users.map(user =>  
      <tr key={user.id}>
        <th scope="row">{user.id}</th>
        <td>{user.username}</td>
        <td>{user.active?
        <Button size="sm" color="primary" onClick = {() =>_revokeAccess(user.id,user.active)} >{user.role.name}</Button>:
        <Button size="sm" color="primary" onClick = {() =>_revokeAccess(user.id,user.active)} >{user.role.name}</Button>
        }
        </td>
      </tr>
      );
      if (isLoading) {
        return (
        <div className = "App1">
          <header className = "App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
        </div>);
      }
      return (
      <div>
        <header className = "Back">
        <div>
        <NavigationBar/>
        </div>
        <div>
          <Table dark>
            <thead>
              <tr>
                <th>Id</th>
                <th>User</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            {elemetUsers}
            </tbody>
          </Table>
        </div>
        </header>
        </div>
        );
      }
}
   
export default withRouter(withCookies(Users));

