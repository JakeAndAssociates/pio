import React, {Component} from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { withCookies ,Cookies} from 'react-cookie';
import  NavigationBar  from './NavigationBarComponent';
import {instanceOf} from "prop-types";
import { Container, Col, Form,FormGroup, Label, Input,Button} from 'reactstrap';


class PaintingUpload extends React.Component {

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      painting: {
        name: null,
        price: null,
        era: null,
        movement: null,
        current: null,
        painter: null
      },
      data: null
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.paintingUpload = this.paintingUpload.bind(this)
  }

  componentDidMount(){
    const { cookies } = this.props;
    let painting = {...this.state.painting};
    painting["painter"] = cookies.get("user");
    this.setState({
      painting
    });
  }

  async onFormSubmit(e){
    e.preventDefault();
    const { cookies } = this.props;

    this.paintingUpload(this.state.data).then((response)=> {
      console.log(response);
      let id = response.data.payload;
        this.paintingUploadSpecifics(this.state.painting, id).then((response) => {
          if(response.status !== 201) {
            let body = response.json();
            fetch('/api/deletePainting/' + body.payload, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                }
              });
          }
          else{
            this.props.history.push('/paintings');
          }
        });
    });
  }

  _onChange = (e) => {
    const { name, value} = e.currentTarget;
    let painting = {...this.state.painting};
    painting[name] = value;
    this.setState({
      painting
    });
  };

  onChange(e) {
    e.preventDefault();
    let data = e.target.files[0];

    this.setState({
          data
    });
  }

  paintingUpload(painting) {

    const url = '/api/uploadPainting';
    const formData = new FormData();
    formData.append('painting', painting);
    console.log(painting);
  
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };

    return  axios.post(url, formData,config)
  }

  paintingUploadSpecifics(painting, id) {
    const url = '/api/uploadPaintingSpecifics/' + id;
    console.log(painting);
    const config = {
      headers: {
        'content-type': 'application/json'
      }
    };
    return  axios.post(url, JSON.stringify(painting),config)
  }

  render() {
    const {_onChange} = this;
    return (

        <div>
        <header className = "Back">
        <div>
          <NavigationBar/>
        </div>
      <form onSubmit={this.onFormSubmit}>
        <div className='padding-file'>
        <h3>Painting Upload</h3>
        </div>
        <div>
          <Col>
            <FormGroup>
              <Label>Name</Label>
              <Input
                  name = "name"
                  placeholder = "name"
                  onChange = {_onChange}
                  style = {{width : 300 }}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Price</Label>
              <Input
                  name = "price"
                  placeholder = "price"
                  onChange = {_onChange}
                  style = {{width : 300 }}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Era</Label>
              <Input
                  name = "era"
                  placeholder = "era"
                  onChange = {_onChange}
                  style = {{width : 300 }}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Current</Label>
              <Input
                  name = "current"
                  placeholder = "current"
                  onChange = {_onChange}
                  style = {{width : 300 }}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Movement</Label>
              <Input
                  name = "movement"
                  placeholder = "movement"
                  onChange = {_onChange}
                  style = {{width : 300 }}
              />
            </FormGroup>
          </Col>
        </div>
        <div className='padding-form'>
        <input type="file" onChange={this.onChange} />
        <button type="submit">Upload</button>
        </div>
      </form>
      </header>
      </div>
   )
  }
}



export default withRouter(withCookies(PaintingUpload))
