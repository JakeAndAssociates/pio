import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Col, Form,FormGroup, Label, Input,Button} from 'reactstrap';
import './Login.css';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';
import { Link } from 'react-router-dom';


export class Login extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
      };

    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            message:'',
            user:{
                username:'',
                password:'',
            },
        };
    }
    
    _onChange = (e) => {
        const { name, value} = e.currentTarget;
        let user = {...this.state.user};
        user[name] = value; 
        this.setState({
            user
        });
    };
    _verify(){
        if(this.state.user.username === ''){
            return false;
        }
        if(this.state.user.password === ''){
            return false;
        }
        return true;
    }

    _onSubmit = async (e) =>  {
        e.preventDefault();
        const {user} = this.state;
        const { cookies } = this.props;
        const request = {
            accessToken:'',
            username : user.username,
            password : user.password, 
        }
        let message = '';
        if(this._verify()){
            const response = await fetch('/api/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request),
                });
            
            if(response.status === 200){
                let body = await response.json();
                if(body.active){
                    cookies.set('accessToken', body.accessToken, { path: '/' });
                    cookies.set('username', body.user, { path: '/' });
                    cookies.set('user', body.payloads[0], { path: '/' });
                    cookies.set('userType', body.payloads[1], {path: '/'});
                    this.props.history.push('/home');
            }
                else{
                    message = "Access Revoked";
                }
            }   
            else{
                message = "Invalid username or password";
            }
        }
        if(this._isMounted){
            this.setState({
                message : message,
            });
        }   
    }
    
    componentDidMount(){
        this._isMounted = true
    }

    componentWillUnmount(){
        this._isMounted = false
    }

    render(){
        const { _onChange, _onSubmit } = this;
        const { user } = this.state;

        return(
            <div className = "background">
            <Container className="App">
            <h2>Login</h2>
            <Form className="form" onSubmit={_onSubmit}>
          <Col>
            <FormGroup>
              <Label>Email</Label>
              <Input
                 name = "username"
                 value = {user.username}
                 onChange = {_onChange}
                 placeholder = "username"
                 style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <Col>
            <FormGroup>
              <Label>Password</Label>
              <Input
                type = "password"
                name = "password"
                value = {user.password}
                onChange = {_onChange}
                placeholder = "password"
                style = {{width : 300 }}
              />
            </FormGroup>
            </Col>
            <div>
            {this.state.message}
            </div>
            <Button type="submit">Submit</Button>
            <Button color="link" tag={Link} to="/register">Sign In</Button>
        </Form>
      </Container>
      </div>
        );
    }
}


export default withRouter(withCookies(Login));
