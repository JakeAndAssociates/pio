import React, { Component } from 'react';
import { Collapse,Nav, Navbar, NavbarBrand, NavbarToggler, DropdownMenu,UncontrolledDropdown, DropdownToggle,DropdownItem} from 'reactstrap';
import { Link , withRouter} from 'react-router-dom';
import { withCookies ,Cookies} from 'react-cookie';
import { instanceOf } from 'prop-types';

export class NavigationBar extends Component {

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {isOpen: false,
    };
  }


  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  logout = ()=> {
    const { cookies } = this.props;
    cookies.remove('accessToken',{ path: '/' });
    cookies.remove('userType',{ path: '/' });
    cookies.remove('user',{ path: '/' });
    this.props.history.push('/');
  }


  render() {

    const {toggle,logout} = this;
    const { cookies } = this.props;

    return (
    <div>
    <Navbar color="dark" dark expand="md">
      <NavbarBrand tag={Link} to="/home">Home</NavbarBrand>
      {cookies.get('userType') === 'ADMIN'?<NavbarBrand tag={Link} to="/users">Users</NavbarBrand>:''}
      <NavbarToggler onClick={toggle}/>
      <NavbarBrand tag={Link} to="/painters">Painter</NavbarBrand>
      <NavbarBrand tag={Link} to="/paintings">Paintings</NavbarBrand>
      <Collapse isOpen={this.state.isOpen} navbar>
        <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                {cookies.get('username')}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem href="/user">
                    Account details
                  </DropdownItem>
                  <DropdownItem href="/updatepwd">
                    Update Password
                  </DropdownItem>
                  <DropdownItem href="/updateprf">
                    Update Profile
                  </DropdownItem>
                  <DropdownItem href="/addFunds">
                    Add Funds
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem onClick = {logout}>
                    Logout
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
        </Nav>
      </Collapse>
    </Navbar>
    </div>);
  }
}
export default withRouter(withCookies(NavigationBar));
