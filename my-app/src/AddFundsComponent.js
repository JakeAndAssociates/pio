import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Col, Form,FormGroup, Label, Input,Button } from 'reactstrap';
import './App.css';
import  NavigationBar  from './NavigationBarComponent';
import { instanceOf } from 'prop-types';
import { withCookies ,Cookies} from 'react-cookie';


export class AddFunds extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    emptyCard = {
        number: '',
        owner: '',
        expiration: '',
        cvc: '',
        amount: 0
    };

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            card: {
                number: '',
                owner: '',
                expiration: '',
                cvc: '',
                amount: 0
            },
        }
    }

    _onChange = (e) => {
        const { name, value} = e.currentTarget;
        let card = {...this.state.card};
        card[name] = value;
        this.setState({
            card
        });
    };

    _verify() {
        if(this.state.card.number === '' || this.state.card.owner === '' || this.state.expiration === '' || this.state.cvc === '' || this.state.amount < 0){
            return false;
        }
        return true;
    }

    _onSubmit = async (e) =>  {
        e.preventDefault();
        const {card} = this.state;
        const {cookies} = this.props;
        const request = {
            username : cookies.get('username'),
            password : cookies.get('user')['password'],
            amount : card.amount
        };
        
        if(this._verify()) {
            const response = await fetch('/api/addFunds', {
                method: 'POST',
                headers: {
                    'accessToken' : cookies.get('accessToken')?cookies.get('accessToken'):'',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request),
            });

            if(response.status !== 401) {
                let body = await response.json();
                this.setState({
                    message : body.errorMessage,
                    card : this.emptyCard
                });
            } else {
                this.props.history.push('/')
            }
        }
        else {
            this.setState({
                message :' Empty!'
            });
        }
    };

    render(){
        const { _onChange, _onSubmit } = this;
        const { card } = this.state;

        return(
            <div className = "background">
                <NavigationBar/>
                <Container className="App">
                    <h2>Add Funds</h2>
                    <Form className="form" onSubmit={_onSubmit}>
                        <Col>
                            <FormGroup>
                                <Label>Card Number</Label>
                                <Input
                                    name = "number"
                                    value = {card.number}
                                    onChange = {_onChange}
                                    placeholder = "card number"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Card Owner</Label>
                                <Input
                                    name = "owner"
                                    value = {card.owner}
                                    onChange = {_onChange}
                                    placeholder = "card owner"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Card Expiration Date</Label>
                                <Input
                                    name = "expiration"
                                    value = {card.expiration}
                                    onChange = {_onChange}
                                    placeholder = "card expiration date"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Card CVC</Label>
                                <Input
                                    name = "cvc"
                                    value = {card.cvc}
                                    onChange = {_onChange}
                                    placeholder = "card cvc"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label>Amount of funds</Label>
                                <Input
                                    name = "amount"
                                    value = {card.amount}
                                    onChange = {_onChange}
                                    placeholder = "amount of funds"
                                    style = {{width : 300 }}
                                />
                            </FormGroup>
                        </Col>
                        <div>
                            {this.state.message}
                        </div>
                        <Button type="submit">Change</Button>
                    </Form>
                </Container>
            </div>
        );
    }
}


export default withRouter(withCookies(AddFunds));
