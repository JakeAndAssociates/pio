import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {instanceOf} from 'prop-types';
import {Link} from 'react-router-dom';
import {withCookies, Cookies} from 'react-cookie';
import {Table, Button, Label} from 'reactstrap';
import NavigationBar from './NavigationBarComponent';
import logo from './logo.svg';
import './App.css';

export class Painters extends Component {

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            painters: [],
            isLoading: true,
            sorting: {
                id: 1,
                username: 1,
                amount: 1
            }
        }
    }

    async componentDidMount() {
        const response = await fetch('/api/painters', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        if (response.status === 200) {
            let body = await response.json();
            this.setState({painters: body.payloads, isLoading: false});
        } else {
            this.props.history.push('/')
        }
    }

    _onClick = async (value) => {
        let sorting = this.state.sorting;
        let painters = this.state.painters.sort((a, b) => sorting[value] * (a[value] > b[value] || b[value] === null ? -1 : 1));

        sorting[value] = -sorting[value];
        this.setState({painters: painters, sorting: sorting});
    };

    render() {
        const {painters, isLoading} = this.state;
        const {_onClick} = this;

        const elementPainters = painters.map(painter =>
            <tr key={painter.id}>
                <th scope="row">{painter.id}</th>
                <td>{painter.username}</td>
                <th>{painter.amount}</th>
                <td><Button size="sm" color="primary" tag={Link} to={{pathname: `/painter/`+ painter.id}}>See Paintings</Button></td>
            </tr>
        );
        if (isLoading) {
            return (
                <div className="App1">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo"/>
                    </header>
                </div>);
        }

        return (
            <header className="Back">
                <div>
                    <NavigationBar/>
                </div>
                <div>
                    <Table dark>
                        <thead>
                        <tr>
                            <th><Label onClick={() => _onClick("id")}>Id</Label></th>
                            <th><Label onClick={() => _onClick("username")}>Name</Label></th>
                            <th><Label onClick={() => _onClick("amount")}>Paintings Sold</Label></th>
                            <th><Label>Action</Label></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {elementPainters}
                        </tbody>
                    </Table>
                </div>
            </header>
        );
    }
}

export default withRouter(withCookies(Painters));

