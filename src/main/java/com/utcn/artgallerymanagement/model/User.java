package com.utcn.artgallerymanagement.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    @Column(unique=true)
    private String username;
    @NonNull
    private String password;
    @NonNull
    private Double amount;
    @NonNull
    private String address;
    @NonNull
    private String phone;
    @NonNull
    private boolean active;
    @ManyToOne
    private Role role;
}
