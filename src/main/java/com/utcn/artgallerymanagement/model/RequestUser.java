package com.utcn.artgallerymanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class RequestUser {

    private Long id;
    private String username;
    private String password;
    private String newPassword;
    private String newAddress;
    private String newPhone;
    private Role role;
    private Double amount;
}