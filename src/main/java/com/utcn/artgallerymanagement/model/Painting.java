package com.utcn.artgallerymanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Painting {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Double price;
    private String era;
    private String current;
    private String movement;

    private String type;

    @ManyToOne
    private User painter;

    @ManyToOne
    private User buyer;


    @Lob
    private byte[] data;

}