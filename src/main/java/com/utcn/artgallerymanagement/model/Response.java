package com.utcn.artgallerymanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Response<T> {

    private String accessToken;
    private String user;
    private String errorMessage;
    private Boolean active;
    private T payload;
    private List<T> payloads;

}