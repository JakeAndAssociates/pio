package com.utcn.artgallerymanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class RequestPainting {

    private Long id;
    private String name;
    private Double price;
    private String era;
    private String current;
    private String movement;
    private User painter;


}