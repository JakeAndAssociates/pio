package com.utcn.artgallerymanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MyPaintingNotFoundException extends RuntimeException {
    public MyPaintingNotFoundException(String message) {
        super(message);
    }

    public MyPaintingNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}