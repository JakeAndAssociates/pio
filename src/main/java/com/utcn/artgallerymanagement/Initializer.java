package com.utcn.artgallerymanagement;

import com.utcn.artgallerymanagement.model.Role;
import com.utcn.artgallerymanagement.repository.PaintingRepository;
import com.utcn.artgallerymanagement.repository.RoleRepository;
import com.utcn.artgallerymanagement.repository.TokenRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
class Initializer implements CommandLineRunner {

    private RoleRepository roleRepository;
    private TokenRepository tokenRepository;
    private PaintingRepository paintingRepository;

    public Initializer(RoleRepository roleRepository, TokenRepository tokenRepository, PaintingRepository paintingRepository) {
        this.roleRepository = roleRepository;
        this.tokenRepository = tokenRepository;
        this.paintingRepository = paintingRepository;
    }

    @Override
    public void run(String... strings) {

        Role role1 = new Role().builder()
                .name("ADMIN")
                .build();

        Role role2 = new Role().builder()
                .name("USER")
                .build();

        Role role3 = new Role().builder()
                .name("PAINTER")
                .build();

       if(roleRepository.findRoleByName("ADMIN") == null)
            roleRepository.save(role1);

        if(roleRepository.findRoleByName("USER") == null)
            roleRepository.save(role2);

        if(roleRepository.findRoleByName("PAINTER") == null)
            roleRepository.save(role3);

    }

}