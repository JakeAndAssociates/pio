package com.utcn.artgallerymanagement.repository;

import com.utcn.artgallerymanagement.model.Role;
import com.utcn.artgallerymanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserById(Long id);
    User findUserByUsernameAndPassword(String username, String password);
    User findUserByUsername(String username);
    List<User> findUserByRole(Role role);
}

