package com.utcn.artgallerymanagement.repository;

import com.utcn.artgallerymanagement.model.Token;
import com.utcn.artgallerymanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findTokenByUser(User user);
    Token findByAccessToken(String accessToken);
}