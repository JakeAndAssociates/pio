package com.utcn.artgallerymanagement.repository;



import com.utcn.artgallerymanagement.model.Painting;
import com.utcn.artgallerymanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaintingRepository extends JpaRepository<Painting, Long> {
    Painting findPaintingById(Long id);
    List<Painting> findPaintingsByPainter(User painter);

    @Query(value = "SELECT p from Painting p where p.buyer = :buyer or p.buyer = null")
    List<Painting> findPaintingsByBuyer(@Param("buyer")User buyer);

    @Query(value = "SELECT COUNT (p) from Painting p where p.painter = :painter and p.buyer <> null")
    Double findPaintingsSoldByPainter(@Param("painter")User painter);
}