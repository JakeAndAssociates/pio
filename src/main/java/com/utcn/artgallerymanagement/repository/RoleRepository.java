package com.utcn.artgallerymanagement.repository;


import com.utcn.artgallerymanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Long> {
    Role findRoleByName(String role);
}