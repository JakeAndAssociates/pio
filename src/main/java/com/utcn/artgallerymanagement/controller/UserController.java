package com.utcn.artgallerymanagement.controller;

import com.utcn.artgallerymanagement.model.*;
import com.utcn.artgallerymanagement.repository.PaintingRepository;
import com.utcn.artgallerymanagement.repository.RoleRepository;
import com.utcn.artgallerymanagement.repository.TokenRepository;
import com.utcn.artgallerymanagement.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    private UserRepository userRepository;
    private PaintingRepository paintingRepository;
    private TokenRepository tokenRepository;
    private RoleRepository roleRepository;
    private final Logger log = LoggerFactory.getLogger(UserController.class);

    public UserController(PaintingRepository paintingRepository, UserRepository userRepository, TokenRepository tokenRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.roleRepository = roleRepository;
        this.paintingRepository = paintingRepository;
    }


    @GetMapping("/users")
    ResponseEntity<?> getUsers(@RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if (token != null) {
            log.info("Request to get users: {}", userRepository.findAll());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new Response<User>().builder()
                            .accessToken(token.getAccessToken())
                            .payloads(new ArrayList<>(userRepository.findAll()))
                            .build());
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }


    @PostMapping("/user")
    ResponseEntity<?> getUser(@Valid @RequestBody RequestUser request, @RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if (token != null) {
            log.info("Request to get user: {}", request.getUsername());
            User result = userRepository.findUserByUsername(request.getUsername());
            if (result != null) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new Response<User>().builder()
                                .payload(result)
                                .build());
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/painters")
    ResponseEntity<?> getPainters() {
        Role painterRole = roleRepository.findRoleByName("Painter");

        List<User> results = new ArrayList<>(userRepository.findUserByRole(painterRole));

        for(int i = 0; i < results.size(); ++i)
            results.get(i).setAmount(paintingRepository.findPaintingsSoldByPainter(results.get(i)));

        return ResponseEntity.status(HttpStatus.OK)
                .body(new Response<Painting>().builder()
                        .payloads(new ArrayList<>(results))
                        .build());
    }
}