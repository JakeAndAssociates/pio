package com.utcn.artgallerymanagement.controller;

import com.utcn.artgallerymanagement.model.*;
import com.utcn.artgallerymanagement.repository.PaintingRepository;
import com.utcn.artgallerymanagement.repository.RoleRepository;
import com.utcn.artgallerymanagement.repository.TokenRepository;
import com.utcn.artgallerymanagement.repository.UserRepository;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class PaintingController {

    private PaintingRepository PaintingRepository;
    private UserRepository userRepository;
    private TokenRepository tokenRepository;
    private RoleRepository roleRepository;
    private final Logger log = LoggerFactory.getLogger(PaintingController.class);

    public PaintingController(PaintingRepository PaintingRepository, TokenRepository tokenRepository, RoleRepository roleRepository, UserRepository userRepository) {

        this.PaintingRepository = PaintingRepository;
        this.tokenRepository = tokenRepository;
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }


    @GetMapping("/Painting")
    ResponseEntity<?> getPaintings() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new Response<Painting>().builder()
                        .payloads(new ArrayList<>(PaintingRepository.findAll()))
                        .build());
    }

    @GetMapping("/Paintings/{id}")
    ResponseEntity<?> getPaintingsByPainter(@PathVariable Long id) {
        User painter = userRepository.findUserById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .body(new Response<Painting>().builder()
                        .payloads(new ArrayList<>(PaintingRepository.findPaintingsByPainter(painter)))
                        .build());
    }

    @GetMapping("/ViewablePainting/{id}")
    ResponseEntity<?> getPaintingsByBuyer(@PathVariable Long id) {

        User buyer = userRepository.findUserById(id);

        return ResponseEntity.status(HttpStatus.OK)
                .body(new Response<Painting>().builder()
                        .payloads(new ArrayList<>(PaintingRepository.findPaintingsByBuyer(buyer)))
                        .build());
    }

    @GetMapping("/Painting/{id}")
    ResponseEntity<?> getPainting(@PathVariable Long id, @RequestHeader("accessToken") String accessToken) {
        log.info("Request to verify token : {}", accessToken);
        Token token = tokenRepository.findByAccessToken(accessToken);
        if (token != null) {
            Optional<Painting> Painting = PaintingRepository.findById(id);
            log.info("Request to get Painting : {}", Painting);
            return Painting.map(response -> ResponseEntity.ok().body(response))
                    .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PostMapping("/uploadPainting")
    ResponseEntity<?> uploadPainting(@RequestParam("painting") MultipartFile painting) throws URISyntaxException {
        Painting result = new Painting();
        try {
            result = PaintingRepository.save(new Painting().builder()
                    .name("none")
                    .price(0.0)
                    .era("none")
                    .current("none")
                    .movement("none")
                    .painter(null)
                    .data(painting.getBytes())
                    .type(painting.getContentType())
                    .buyer(null)
                    .build());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.created(new URI("/api/painting/" + result.getId()))
                .body(new Response<String>().builder()
                        .payload(result.getId())
                        .build());
    }

    @PostMapping("/uploadPaintingSpecifics/{id}")
    ResponseEntity<Painting> uploadPaintingSpecifics(@Valid @RequestBody RequestPainting Painting, @PathVariable Long id) throws URISyntaxException {
        Painting painting = PaintingRepository.findPaintingById(id);
        painting.setName(Painting.getName());
        painting.setEra(Painting.getEra());
        painting.setCurrent(Painting.getCurrent());
        painting.setMovement(Painting.getMovement());
        painting.setPrice(Painting.getPrice());
        painting.setPainter(Painting.getPainter());

        PaintingRepository.save(painting);

        return ResponseEntity.created(new URI("/api/painting/" + painting.getId()))
                .body(painting);
    }

    @PostMapping("/deletePainting/{id}")
    ResponseEntity<?> deletePainting(@PathVariable Long id) throws URISyntaxException {

        PaintingRepository.deleteById(id);
        return ResponseEntity.created(new URI("/api/painting/"))
                .body(new Response<String>().builder()
                        .errorMessage("saving failed")
                        .build());
    }

    @GetMapping("/downloadPainting/{id}")
    ResponseEntity<?> downloadPainting(@PathVariable Long id) {
        Painting painting = PaintingRepository.findPaintingById(id);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(painting.getType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + painting.getName() + "\"")
                .body(new ByteArrayResource(painting.getData()));
    }

    @PostMapping("/buyPainting/{paintingId}/{userId}")
    ResponseEntity<?> buyPainting(@PathVariable("paintingId") Long paintingId, @PathVariable("userId") Long userId) throws URISyntaxException {

        Painting painting = PaintingRepository.findPaintingById(paintingId);
        User buyer = userRepository.findUserById(userId);

        buyer.setAmount(buyer.getAmount() - painting.getPrice());
        painting.setBuyer(buyer);

        if(buyer.getAmount() > 0.0) {
            PaintingRepository.save(painting);
            userRepository.save(buyer);
        }

        return ResponseEntity.created(new URI("/api/painting/"))
                .body(new Response<String>().builder()
                        .errorMessage("saving failed")
                        .build());
    }

}
