package com.utcn.artgallerymanagement.sevice;

import com.utcn.artgallerymanagement.exception.MyPaintingNotFoundException;
import com.utcn.artgallerymanagement.model.Painting;
import com.utcn.artgallerymanagement.repository.PaintingRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PaintingStorageService implements GenericService{

    @Autowired
    private PaintingRepository PaintingRepository;

    public Painting getPainting(Long id) {
        return PaintingRepository.findById(id).
                orElseThrow(() -> new MyPaintingNotFoundException("File not found with id " + id));
    }
}

