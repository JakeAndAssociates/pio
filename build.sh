#!/bin/bash

echo "echo '127.0.0.1 device_management' >> /etc/hosts\""

# Kill all backgrounded processes on exit.
trap "pkill -P $$" EXIT

# Create or start MySql container
docker start device_management || docker run \
  -p 3306:3306 \
  --name device_management \
  -e MYSQL_ROOT_PASSWORD=my-secret \
  -e MYSQL_PASSWORD=some_pass \
  -e MYSQL_USER=user \
  -e MYSQL_DATABASE=device_management \
  -d \
  mysql:latest

# Start
docker start device_management

# Add the   specific tables to the database
#for f in schema/*.sql; do
  #docker exec -i device_management mysql -u user -psome_pass device_management < "$f";
#done

# Run shell on container for manual database test
# docker exec -it dm /bin/bash

# Build new image form project
./gradlew build

# Stop outdated container
docker stop device-management

# Remove outdated container
docker rm device-management

# Remove image
docker rmi device-management

# Create new image
docker build . -t device-management

# Create new container
docker run -p 8083:8083 --name device-management --link device_management:mysql -d device-management
