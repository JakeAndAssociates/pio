# Art_Gallery

Ce trebuie sa faci:
1. Tehnologii:
* Docker (optional)
* Yarn
* npm
* Idea
2. Clone repository
3. In folderul de priect 
```
yarn create react-app app
cd app
yarn add bootstrap@4.1.3 react-cookie@3.0.4 react-router-dom@4.3.1 reactstrap@6.5.0
```
4. Copiaza tot ce este in my-app/src in app/src si 
```
yarn start
```
5. Frontend-ul este activ pe localhost:3000/api/